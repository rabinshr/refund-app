import React, {Component} from 'react';

class Order extends Component {
    render() {
        return (
            <tr>
                <td>{this.props.order.id}</td>
                <td>{this.props.order.total_inc_tax}</td>
                <td>{this.props.order.refunded_amount}</td>
                <td></td>
            </tr>
        );
    }
}

export default Order;