import React, { Component } from 'react'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import { Link } from 'react-router-dom'
import { apiFetch } from './api-fetch'

class ListComponent extends Component {
    constructor(props) {
        super(props);
        console.log('i am in list constructor');
        console.log(props)
        // this.listOrders = this.listOrders.bind(this);
        this.state = {
            ordersData: null,
            loading: true
        }

        apiFetch(['/v2/orders?sort=date_created:desc&limit=10'])
            .then(([ordersData]) => {
                this.setState({ ordersData: ordersData})

                this.setState({loading: false});
            }
        );
    }

    _getOrdersData(ordersData)
    {
        return ordersData.map(order => {
            var linkHtml = ''
            if (order.total_inc_tax !== order.refunded_amount) {
                linkHtml = <Link className="btn btn-primary" to={"order/" + order.id }>Refund</Link>
            }

            var statusHtml = <span className="badge badge-info">{order.status}</span>
            if (order.status === 'Refunded' || order.status === 'Partially Refunded') {
                statusHtml = <span className="badge badge-success">{order.status}</span>
            }

            return {
                'id': order.id,
                'total': order.total_inc_tax,
                'status': statusHtml,
                'refunded_amount': order.refunded_amount,
                'order_view_link': linkHtml
            }
        }
        );
    }

    render() {
        if (!this.state.loading) {
            const data = this._getOrdersData(this.state.ordersData);

            const columns = [
                {
                    Header: 'Id',
                    accessor: 'id'
                }, {
                    Header: 'Status',
                    accessor: 'status'
                }, {
                    Header: 'Order Total',
                    accessor: 'total',
                }, {
                    Header: 'Refunded amount',
                    accessor: 'refunded_amount',
                }, {
                    Header: '',
                    accessor: 'order_view_link'
                }
            ];

            return (
                <div className="container">
                    <p className="lead">List of Orders</p>

                    <ReactTable
                    data={data}
                    columns={columns}
                    />
                </div>
            )
        } else {
            return (
                <div className="container">
                    <div className="d-flex justify-content-center m-5">
                        <div className="spinner-border" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default ListComponent;
