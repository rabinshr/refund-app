import React, { Component } from 'react';
import Payment from './payment'

class RefundMethod extends Component {
  constructor(props) {
    super(props);

    this.method = props.method
    this.index = props.index
    this.handler = props.handler
  }

  render() {
      return (
          <div className="form-check">
            <input className="form-check-input" type='radio' name='refund_method' value={this.index} onChange={this.handler} />
            {this.method.map((payment, i) => {
                return (<Payment key={i} provider={payment.provider_description} amount={payment.amount}
                    offline={payment.is_offline}
                   />)
           })}
        </div>
    )
    }
}

export default RefundMethod;
