import React, { Component } from 'react';

class Payment extends Component {
  constructor(props) {
    super(props);

    this.provider = props.provider
    this.amount = props.amount
    this.isOffline = props.offline
  }

  render() {
      return (
          <label className="form-check-label">{this.provider}: ${this.amount}</label>
    )
    }
}

export default Payment;
