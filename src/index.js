import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Route} from 'react-router-dom'
import ListComponent from './ListComponent'
import OrderItemComponent from './OrderItemComponent'
import 'bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render((
    <BrowserRouter>
        <div>
        <Route path="/" component={App} />
        <Route path="/list" component={ListComponent} />
        <Route path="/order/:orderid" component={OrderItemComponent} />
        </div>
      </BrowserRouter>
  ), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
