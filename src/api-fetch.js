const storeHash = 'e6bw1jqg1n';
export function apiFetch(urls) {
    const properties = {
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-Auth-Client': 'p6y99w2lsvpxeydw7lvhcf3ujubfjxj',
            'X-Auth-Token': 'dosjwy7d2jl7koh5zovhq6o7tn4dzv3'
        }
    };

    return Promise.all(
        urls.map(url => {
            const proxyurl = "https://cors-anywhere.herokuapp.com/";
            url = `https://api.staging.zone/stores/` + storeHash + url;
            return fetch(proxyurl + url, properties)
                .then(data => data.json())
                .catch(e => e)
        }
    ))
}

export function apiPost(url, data) {
    const properties = {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-Auth-Client': 'p6y99w2lsvpxeydw7lvhcf3ujubfjxj',
            'X-Auth-Token': 'dosjwy7d2jl7koh5zovhq6o7tn4dzv3'
        },
        body: JSON.stringify(data)
    };


    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    url = `https://api.staging.zone/stores/` + storeHash + url;
    return fetch(proxyurl + url, properties)
        .then(data => data.json())
        .catch(e => e)
}
