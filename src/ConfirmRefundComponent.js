import React, { Component } from 'react';
import { apiPost } from './api-fetch'
import RefundMethod from './refund-method'
import { Link } from 'react-router-dom'

class ConfirmRefundComponent extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleRefundMethod = this.handleRefundMethod.bind(this)
    this.quote = props.quoteResponse
    this.items = props.items
    this.order = props.order

    this.state = {
        refundMethodIndex: null
    }
  }

  handleRefundMethod(e) {
      this.setState({refundMethodIndex: e.target.value})
  }

  handleSubmit(e) {
      e.preventDefault()
      console.log('handling submit')
      const payload = {
          'items': this.items,
          'payments': this.quote.refund_methods[this.state.refundMethodIndex].map(payment => {
              return {
                'provider_id': payment.provider_id,
                'amount': payment.amount,
                'mark_offline': payment.is_offline
              }
          })
      }
      const submitUrl = `/v3/orders/${this.order}/refunds`
      apiPost(submitUrl, payload)
        .then(submitData => {
            this.setState({ refundSubmit: true})
        })
        .catch(error => {
            console.log(error)
        })
  }

  render() {
      if (this.state.refundSubmit) {
          return (
            <div className="container">
                <div className="alert alert-primary" role="alert">
                    Refund successfully created
                </div>

                <div className="row">
                    <div className="col-sm-9"></div>
                    <div className="col-sm-3 right">
                        <Link className="btn btn-primary" to={"/list"}>Back to Order List page</Link>
                    </div>
                </div>
            </div>
          )
      } else {
          return (
            <div className="container">
                <p className="lead">Confirm Refund for Order {this.order}</p>

                <form onSubmit={this.handleSubmit}>
                    {this.quote.order_level_refund_amount > 0 &&
                        <div className="row">
                            <div className="col-sm-2">Order level Refund Amount</div>
                            <div className="col-sm-2">${this.quote.total_refund_amount}</div>
                        </div>
                    }

                    <div className="row">
                        <div className="col-sm-2">Refund Total</div>
                        <div className="col-sm-2">${this.quote.total_refund_amount}</div>
                    </div>

                    {this.quote.total_refund_tax_amount > 0 &&
                        <div className="row">
                            <div className="col-sm-2">Refund Tax</div>
                            <div className="col-sm-2">${this.quote.total_refund_tax_amount}</div>
                        </div>
                    }

                    <div className="row">
                        <div className="col-sm-2">Refund Method</div>
                        <div className="col-sm-2">
                            {this.quote.refund_methods.map((method, i) => {
                               return (<RefundMethod index={i} key={i} method={method} handler={this.handleRefundMethod} />)
                            })}
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-8"></div>
                        <div className="col-sm-3 right">
                            <Link className="btn btn-secondary" to={"/list" }>Back to List Page</Link>
                        </div>
                        <div className="col-sm-1 right">
                            <button className='btn btn-primary'>Continue</button>
                        </div>
                    </div>
                </form>
            </div>


        )
       }
    }
}

export default ConfirmRefundComponent;
