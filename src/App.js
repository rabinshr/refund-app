import React, { Component } from 'react'
import './App.css'
import { Link } from 'react-router-dom'

class App extends Component {
  constructor(props) {
    super(props);

    this.setProceed = this.setProceed.bind(this)

    this.state = {
      initial: true
    };
  }

  setProceed(e) {
      this.setState({initial: false})
  }


  render() {
    return (
      <div className="App">
        {this.state.initial &&
            <div className="container">
                <div className="d-flex justify-content-center m-5">
                    <h1>Welcome to Refund App</h1>
                </div>
                <div className="row">
                    <div className="col-sm-4"></div>
                    <div className="col-sm-3 right">
                        <Link className="btn btn-primary" to={"/list"} onClick={this.setProceed}>Proceed to Order List</Link>
                    </div>
                </div>
            </div>
        }
      </div>
    )
  }
}

export default App;
