import React, { Component } from 'react';
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {apiFetch, apiPost} from './api-fetch'
import ConfirmRefundComponent from './ConfirmRefundComponent.js'
import { Link } from 'react-router-dom'

class OrderItemComponent extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this)
    this.setProductRefundQuantity = this.setProductRefundQuantity.bind(this)
    this.setWrapRefundQuantity = this.setWrapRefundQuantity.bind(this)
    this.setOrderRefund = this.setOrderRefund.bind(this)

    this.state = {
        orderProductsData: null,
        orderRefundsData: null,
        inputQuantity: [],
        orderRefundAmount: 0,
        productRefund: [],
        wrapRefund: [],
        orderRefund: 0,
        loading: true,
        quoteReceived: false,
        quoteResponse: null
    }
    const orderProductUrl = `/v2/orders/${this.props.match.params.orderid}/products`
    const orderRefundsUrl = `/v3/orders/refunds?order_id:in=${this.props.match.params.orderid}`
    apiFetch([orderProductUrl, orderRefundsUrl])
      .then(([orderProductsData, orderRefundsData]) => {
        const refundProductData = []
        var orderRefundAmount = 0
        orderRefundsData.data.forEach(refund => {
          refund.items.forEach(refundItem => {
            const itemId = refundItem.item_id
            const quantity = refundItem.quantity
            const itemType = refundItem.item_type
            const amount = refundItem.requested_amount
            if (itemType === 'PRODUCT') {
              if (refundProductData[itemId] === undefined) {
                refundProductData[itemId] = 0
              }
              refundProductData[itemId] += quantity
            } else {
              orderRefundAmount += amount
            }
          })
        });

        const data = orderProductsData.map(product => {
          var refundInput = ''
          var refundQuantity = refundProductData[product.id] === undefined ? 0 : refundProductData[product.id]
          if (product.quantity > refundQuantity) {
              refundInput = <input type="number" name={"refund_quantity-" + product.id} onChange={this.setProductRefundQuantity} data-product-id={product.id} />
          }
          // if (product.wrapping_cost_inc_tax > 0) {
          //   refundInput += <input type="number" name={"refund_wrapping-" + product.id} onChange={this.setWrapRefundQuantity} data-product-id={product.id} />
          // }
          return {
            'name': product.name,
            'total': product.total_inc_tax,
            'quantity': product.quantity,
            'refunded_quantity': refundProductData[product.id] === undefined ? 0 : refundProductData[product.id],
            'gift_wrapping': product.wrapping_cost_inc_tax,
            'refund_input': refundInput
          }
        })

        this.setState({ orderProductsData: data, orderRefundAmount: orderRefundAmount, loading: false})
      }
    );
  }

  setProductRefundQuantity(e) {
      const productId = e.target.getAttribute('data-product-id');
      const refundQuantity = e.target.value;
      var productRefund = this.state.productRefund;
      productRefund[productId] = refundQuantity;

      this.setState({ productRefund: productRefund });
  }

  setWrapRefundQuantity(e) {
      const productId = e.target.getAttribute('data-product-id')
      const refundQuantity = e.target.value
      var wrapRefund = this.state.wrapRefund
      wrapRefund[productId] = refundQuantity

      this.setState({ wrapRefund: wrapRefund })
  }

  setOrderRefund(e) {
      this.setState({ orderRefund: e.target.value })
  }

  _createItemPayload() {
      const items = []
      if (this.state.orderRefund > 0) {
          items.push({
              item_id: parseInt(this.props.match.params.orderid),
              item_type: 'ORDER',
              amount: parseFloat(this.state.orderRefund)
          })
      }
      this.state.productRefund.map((quantity, id) => {
          items.push({
              item_id: id,
              item_type: 'PRODUCT',
              quantity: parseInt(quantity)
          })
      })
      this.state.wrapRefund.map((quantity, id) => {
          items.push({
              item_id: id,
              item_type: 'GIFT_WRAP',
              quantity: parseInt(quantity)
          })
      })

      return items
  }

  handleSubmit(e) {
      e.preventDefault()

      const payload = {
          "items": this._createItemPayload()
      }
      const quoteUrl = `/v3/orders/${this.props.match.params.orderid}/refund_quotes`
      apiPost(quoteUrl, payload)
        .then(quoteData => {
            console.log(quoteData.data)
            this.setState({ quoteReceived: true, quoteResponse: quoteData.data})
        })
        .catch(error => {
            console.log(error)
        })
  }

  render() {
    if (this.state.quoteReceived) {
      return (
          <ConfirmRefundComponent
          order={this.props.match.params.orderid}
          quoteResponse={this.state.quoteResponse}
          items={this._createItemPayload()}
           />
      )
    }

    if (!this.state.loading) {
          const columns = [
            {
                Header: 'Item',
                accessor: 'name',
            }, {
                Header: 'Purchased quantity',
                accessor: 'quantity',
                width: 200
            }, {
                Header: 'Refunded quantity',
                accessor: 'refunded_quantity',
                width: 200
            }, {
              Header: 'How many to refund?',
              accessor: 'refund_input',
              width: 200
            }
        ]

        return (
            <div className="container">
                <p className="lead">Refund Order {this.props.match.params.orderid}</p>

                <form onSubmit={this.handleSubmit}>
                  <ReactTable
                    data={this.state.orderProductsData}
                    columns={columns}
                    showPagination={false}
                    minRows={0}
                  />

                  {this.state.orderRefundAmount > 0 &&
                      <div className="row">
                          <div className="col-sm-5">
                              Existing order level refund amount on this order: ${this.state.orderRefundAmount}
                          </div>
                      </div>
                  }
                  <div className="row">
                        <div className="col-sm-3">
                            <label htmlFor='order-refund-input'>Add order level refund amount</label>
                        </div>
                        <div className="col-sm-3">
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">$</span>
                                </div>
                                <input id='order-refund-input' type='number' name='order-refund-input' onChange={this.setOrderRefund} />
                            </div>
                      </div>
                  </div>

                  <div className="row">
                    <div className="col-sm-10"></div>
                    <div className="col-sm-1 right">
                        <Link className="btn btn-secondary" to={"/list"}>Back</Link>
                    </div>
                    <div className="col-sm-1 right">
                    <button className='btn btn-primary'>Continue</button>
                    </div>
                    </div>
                </form>
            </div>
        )
    }

    return (

        <div className="container">
            <div className="d-flex justify-content-center m-5">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    )
  }
}

export default OrderItemComponent;
